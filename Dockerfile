FROM maven:3.6.3-jdk-11

RUN curl -O https://download.docker.com/linux/static/stable/x86_64/docker-19.03.8.tgz \
    && tar xzvf docker-19.03.8.tgz --strip 1 -C /usr/local/bin docker/docker \
    && rm -rf docker-19.03.8.tgz

CMD ["/bin/sh"]
