# docker-maven-pipeline

This module contains a Dockerfile to create a docker image based on the maven:3.6.3-jdk-11 image, with the additional component docker installed.

It is intended to be used in Bitbucket pipelines to activate docker functionality.

## Supported tags

* [`1.0.0`, (*1.0.0/Dockerfile*)](https://bitbucket.org/zuunr/docker-maven-pipeline/src/1.0.0/Dockerfile)

## Usage

To use the image in a Bitbucket pipeline environment and authenticate with your gcloud environment, use the following yaml configuration as a base:

```yaml
- step:
    name: My kubectl step
    # Check the image version tag
    image: zuunr/docker-maven-pipeline:1.0.0
    script:
      # Set the $GCLOUD_API_KEYFILE to the contents of your gcloud service account key file in the Bitbucket pipelines environment variables
      - echo $GCLOUD_API_KEYFILE > ./gcloud-api-key.json
      - docker login -u _json_key -p "$GCLOUD_API_KEYFILE" eu.gcr.io
      - docker push my-image:latest
```
